package pe.everis.wcamposs.test_testng.test;



import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import io.appium.java_client.android.AndroidDriver;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ExampleUnitTest {

    public static AndroidDriver driver;
    WebDriverWait wait;
    @BeforeClass
    public void connectAppium() throws Exception {

        //para local windows
        /*File appDir = new File("./build/outputs/apk/");
        File app = new File(appDir, "app-debug.apk");*/
        try {
            File appDir = new File("build/outputs/apk");
            File app = new File(appDir,"app-debug.apk");

            DesiredCapabilities capabilities = new DesiredCapabilities();
            //which mobile OS to use: Android, iOS or FirefoxOS
            capabilities.setCapability("platformName", "Android");
            capabilities.setCapability("deviceName", "emulator-5554");//LGD72216d44791 //samsung: 988627314c54325234
            //the absolute local path to the APK
            capabilities.setCapability("app",app.getAbsolutePath());
            //Java package of the tested Android app
            capabilities.setCapability("appPackage", "pe.everis.wcamposs.test_testng");
            // activity name for the Android activity you want to run from your package. This need to be preceded by a . (example: .MainActivity)
            capabilities.setCapability("appActivity",".MainActivity");

            // constructor to initialize driver object
            driver = new AndroidDriver(new URL("http://10.232.101.206:4723/wd/hub"), capabilities);//10.232.100.196
            driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
            wait = new WebDriverWait(driver, 120, 50);
        }catch (IOException e){
            e.printStackTrace();
        }

    }

    @AfterClass
    public void testCaseDown(){
        if(driver !=null){
            driver.quit();
        }
    }
    @Test
    public void testVerifyElements() throws Exception {
        System.out.print("iniciando Test");

        WebElement strUser = driver.findElement(By.id("pe.everis.wcamposs.testappium:id/txtUsuario"));
        strUser.sendKeys("everis");
        System.out.print("\n se ingreso el user");
        WebElement strPass = driver.findElement(By.id("pe.everis.wcamposs.testappium:id/txtClave"));
        strPass.sendKeys("123");
        System.out.print("\n se ingreso la clave");

        WebElement btnAceptar = driver.findElement(By.id("pe.everis.wcamposs.testappium:id/btnLogin"));
        btnAceptar.click();
        System.out.print("\n hizo clic en btn");

        WebElement strMsj = driver.findElement(By.id("pe.everis.wcamposs.testappium:id/txtMensaje"));
        strMsj.getText();
        System.out.print("\n Resultado: "+strMsj.getText());

    }
}